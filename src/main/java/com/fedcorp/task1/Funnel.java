package com.fedcorp.task1;

@FunctionalInterface
public interface Funnel {
    int execute (int a, int b, int c);
}
