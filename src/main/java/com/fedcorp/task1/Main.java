package com.fedcorp.task1;

import java.util.OptionalInt;
import java.util.stream.IntStream;

public class Main{
    public static void main(String[] args) {

        Funnel fun1 = (a,b,c)-> {
            OptionalInt o = IntStream.of(a,b,c).max();
            if(o.isPresent()) return o.getAsInt();
            return 0;
        };

//      Funnel fun10 = (a,b,c)->(a>=b)? (a>=c)? a:c :(b>=c)? b:c   Альтернативний метод реалізації

        System.out.println(fun1.execute(4,12,25));
        Funnel fun2 = (a,b,c)-> a+b+c;
        System.out.println(fun2.execute(4,12,44));

    }
}
