package com.fedcorp.task4;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class SomeApplication {
    public static void main(String[] args) {
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        List<String> list = new ArrayList();
        String s = "";
        do{
            try {
                s = in.readLine();
                if(!s.isEmpty()) list.add(s);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }while(!(s.isEmpty()));
        try {
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Stream<String> stream = list.stream();

        System.out.println(stream
                .flatMap(v->Stream.of(v.split(" ")))
                .distinct()
                .count()
        );
        System.out.println();
        stream = list.stream();
        stream
                .flatMap(v->Stream.of(v.split(" ")))
                .distinct()
                .sorted(String::compareTo)
                .forEach(v->System.out.println(v))
        ;

//        stream = list.stream();
//        stream
//                .flatMap(v->Stream.of(v.split(" ")))
//                .collect(Collectors.groupingBy( ,Collectors.counting()))

    }
}
