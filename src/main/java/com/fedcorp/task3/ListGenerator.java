package com.fedcorp.task3;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ListGenerator {

    public int counter = 0;

    public List<Integer> getList1(){
        Stream<Integer>  stream = Stream.iterate(100, i-> i+5).limit(11);
        return stream.collect(Collectors.toList());
    }
    public List<Integer> getList2(){
        Stream<Integer>  stream = Stream.of(15,5,4,7,13,5,3);
        return stream.collect(Collectors.toList());
    }
    public List<Integer> getList3(){
        Stream<Integer>  stream = Stream.generate(()->counter++).limit(11);
        return stream.collect(Collectors.toList());
    }

}
