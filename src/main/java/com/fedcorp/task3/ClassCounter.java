package com.fedcorp.task3;

import java.util.List;
import java.util.Optional;
import java.util.OptionalDouble;

public class ClassCounter {

    /**Method display sum of values from given List<Integer> */
    void getSum1(List<Integer> list){
        int tmp = list.stream()
                .mapToInt(n->n)
                .sum();
        System.out.println(tmp);
    }

    /**Method display sum of values from given List<Integer> */
    void getSum2(List<Integer> list){
        int tmp = list.stream()
                .reduce(0, Integer::sum);
        System.out.println(tmp);
    }

    /**Method display average value from given List<Integer>*/
    void getAverage(List<Integer> list){
        OptionalDouble od = list.stream()
                .mapToInt(i->i)
                .average();
        if(od.isPresent()) System.out.println(od.getAsDouble());
    }

    /**Method display minimum value from given List<Integer>*/
    void getMin(List<Integer> list){
        Optional tmp = list.stream()
                .min((o1, o2) -> o1-o2);
        if(tmp.isPresent()) System.out.println(tmp.get());
    }

    /**Method display maximum value from given List<Integer>*/
    void getMax(List<Integer> list){
        Optional tmp = list.stream()
                .max((o1, o2) -> o1-o2);
        if(tmp.isPresent()) System.out.println(tmp.get());
    }

    /**Method display values greater then average from given List<Integer> */
    void countValBigThan(List<Integer> list){
        OptionalDouble od = list.stream()
                .mapToInt(i->i)
                .average();
        if(od.isPresent()) {
            long tmp = list.stream()
                    .filter(v -> v > od.getAsDouble())
                    .count();
            System.out.println(tmp);
        }
    }


}
