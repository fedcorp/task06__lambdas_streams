package com.fedcorp.task2;

public class Activity {
    public void run(StringBuilder s){
        System.out.println(s +" wants to run");
    }
    public void swim(StringBuilder s){
        System.out.println(s +" wants to swim");
    }
    public void walk(StringBuilder s){
        System.out.println(s +" wants to walk");
    }
    public void drive(StringBuilder s){
        System.out.println(s +" wants to drive");
    }

}
