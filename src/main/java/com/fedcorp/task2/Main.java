package com.fedcorp.task2;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Activity a = new Activity();
        StringBuilder s;
        String command;


        Scanner sc = new Scanner(System.in);
        System.out.println("Enter your name");
        s = new StringBuilder(sc.nextLine());
        System.out.println("Choose and type one of this activity:");
        System.out.println("run");
        System.out.println("walk");
        System.out.println("swim");
        System.out.println("drive");
        command = sc.next();

        User user = new User(
                new Run(a, s),
                new Command(){
                    @Override
                    public void execute(){
                        a.swim(s);
                    }
                },
                ()->a.walk(s),
                new Drive(a, s)::execute
        );
        switch (command){
            case "run": user.run();break;
            case "walk": user.walk();break;
            case "swim": user.swim();break;
            case "drive": user.drive();break;
            default:
                System.out.println("Wrong command");
        }

    }
}
