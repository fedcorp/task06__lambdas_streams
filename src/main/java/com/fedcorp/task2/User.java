package com.fedcorp.task2;

public class User {
    private Command run;
    private Command swim;
    private Command walk;
    private Command drive;

    public User(Command run, Command swim, Command walk, Command drive) {
        this.run = run;
        this.swim = swim;
        this.walk = walk;
        this.drive = drive;
    }
    public void run(){
        run.execute();
    }

    public void swim(){
        swim.execute();
    }

    public void walk(){
        walk.execute();
    }

    public void drive(){
        drive.execute();
    }

}
