package com.fedcorp.task2;

public interface Command {
    void execute();
}

class Run implements Command {
    private Activity activity;
    private StringBuilder someString;

    public Run(Activity activity, StringBuilder someString) {
        this.activity = activity;
        this.someString = someString;
    }

    @Override
    public void execute() {
        activity.run(someString);

    }
}

    class Drive implements Command {
        private Activity activity;
        private StringBuilder someString;

        public Drive(Activity activity, StringBuilder someString) {
            this.activity = activity;
            this.someString = someString;
        }

        @Override
        public void execute() {
            activity.drive(someString);

        }
    }
